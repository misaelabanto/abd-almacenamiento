const express = require('express');
const router = express.Router();

function renderView(view, title) {
	return (req, res, next) => {
		res.render(view, {title});	
	}
}

router.get('/seguridad', renderView('menu/seguridad', 'Seguridad'));

router.get('/online/gerencial/parametros/general', renderView('menu/online/gerencial/parametros/general', 'Parámetros generales'));
router.get('/online/gerencial/parametros/catalogo/productos', renderView('menu/online/gerencial/parametros/catalogo/productos', 'Catálogo de productos'));
router.get('/online/gerencial/parametros/catalogo/reglas', renderView('menu/online/gerencial/parametros/catalogo/reglas', 'Catálogo de reglas'));
router.get('/online/gerencial/parametros/catalogo/protocolos', renderView('menu/online/gerencial/parametros/catalogo/protocolos', 'Catálogo de protocolos'));
router.get('/online/gerencial/parametros/catalogo/rutas', renderView('menu/online/gerencial/parametros/catalogo/rutas', 'Configuración de rutas'));

router.get('/online/gerencial/consulta/indicadores', renderView('menu/online/gerencial/consultas/indicadores', 'Indicadores'));

router.get('/online/operativo/dataEntry/nuevaReserva', renderView('menu/online/operativo/dataEntry/nuevaReserva', 'Nueva reserva'));
router.get('/online/operativo/dataEntry/entrada', renderView('menu/online/operativo/dataEntry/entrada', 'Solicitud de entrada'));
router.get('/online/operativo/dataEntry/salida', renderView('menu/online/operativo/dataEntry/salida', 'Solicitud de salida'));

router.get('/online/operativo/consultas/estadoReserva', renderView('menu/online/operativo/consultas/estadoReserva', 'Estado de la reserva'));

router.get('/batch/aplicativo/actualizacion', renderView('menu/batch/aplicativo/actualizacion', 'Actualización de BD'));

module.exports = router;