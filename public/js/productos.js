let costos = {
  'ALM01': {
    costoTemperatura: 2,
    costoPeligrosidad: 0,
    protocolo: {
      costoSolicIngreso: 3,
      costoRecepcion: 2,
      costoTransporte: 1,
      costoPutAway: 2,
      costoSolicRetiro: 3,
      costoPicking: 0,
      costoPacking: 0,
      costoDespacho: 1
    }
  },
  'ALM02': {
    costoTemperatura: 0,
    costoPeligrosidad: 2,
    protocolo: {
      costoSolicIngreso: 3,
      costoRecepcion: 3,
      costoTransporte: 1,
      costoPutAway: 2,
      costoSolicRetiro: 3,
      costoPicking: 2,
      costoPacking: 2,
      costoDespacho: 1
    }
  }
};

function mostrarDetalleCosto(tipoServicio) {
  $('#costoTemperatura').text(costos[tipoServicio].costoTemperatura);
  $('#costoPeligrosidad').text(costos[tipoServicio].costoPeligrosidad);
  let promedioTipoBien = (costos[tipoServicio].costoTemperatura + costos[tipoServicio].costoPeligrosidad)/2;
  $('#promedioTipoBien').text(promedioTipoBien);
  let sum = 0, cont = 0;
  for(let costo in costos[tipoServicio].protocolo) {
    sum += costos[tipoServicio].protocolo[costo];
    cont++;
    $('#' + costo).text(costos[tipoServicio].protocolo[costo]);
  }
  let promedioTipoProtocolo = sum/cont;
  $('#promedioTipoProtocolo').text(promedioTipoProtocolo);
  $('#promedioGeneral').text(` ${(promedioTipoBien + promedioTipoProtocolo)/2} (bajo)`);
  $('.modal').modal('show');
}

