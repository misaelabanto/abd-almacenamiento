$('#btnAgregarTipoUsuario').click(() => {
	$('#tituloModal').text('Agregar tipo de usuario');
	$('.modal').modal('show');
});

$('#btnModificarTipoUsuario').click(() => {
	$('#inputNombreTipoUsuario').parent().hide();
	$('#tituloModal').text('Agregar tipo de usuario');
	$('.modal').modal('show');
})

$('#selectTiposUsuario').change(function() {
	tiposUsuario[$(this).val()].forEach(p => {
		$('#selectPermisosAsignados').append(`<option>${p}</option>`);
	});
});

let tiposUsuario = {
	'Gerente': [
		"Consulta de clientes",
		"Catálogo reglas",
		"Parámetros catálogo bien",
		"Parámetros catálogo contenedor",
		"Parámetros reglas",
	],
	'Administrador de BD': [
		"Seguridad"
	],
	'Cliente': [
		"Nueva reserva",
		"Consulta de reserva"
	]
};

Array.prototype.removeAll = function(array) {
	array.forEach(a => {
		const index = this.indexOf(a);
		this.splice(index, 1);
	});
};

let privilegios = [
	"Catálogo de clientes",
	"Catálogo reglas",
	"Estado de reserva del bien",
	"Indicadores Gerenciales",
	"Nueva reserva",
	"Consulta de reserva",
	"Parámetros catálogo bien",
	"Parámetros catálogo contenedor",
	"Parámetros reglas",
	"Registro de packing",
	"Reportes de contenedor",
	"Seguridad"
];

let asignadosPorCrear = [];

$(() => {
	actualizarPrivilegios();
});

function actualizarPrivilegios() {
	let selectPrivilegios = $('#selectPrivilegiosGlobales').html('');
	privilegios.forEach(p => {
		selectPrivilegios.append(`<option>${p}</option>`);
	});
}

function actualizarPrivilegiosAsignados() {
	let selectPrivilegios = $('#selectPrivilegiosAsignadosPorCrear').html('');
	asignadosPorCrear.forEach(p => {
		selectPrivilegios.append(`<option>${p}</option>`);
	});
}

$('#btnAsignar').click(() => {
	asignadosPorCrear = asignadosPorCrear.concat($('#selectPrivilegiosGlobales').val());
	privilegios.removeAll($('#selectPrivilegiosGlobales').val());
	actualizarPrivilegiosAsignados();
	actualizarPrivilegios();
});

$('#btnQuitar').click(() => {
	privilegios = privilegios.concat($('#selectPrivilegiosAsignadosPorCrear').val());
	asignadosPorCrear.removeAll($('#selectPrivilegiosAsignadosPorCrear').val());
	actualizarPrivilegios();
	actualizarPrivilegiosAsignados();
});

$('#confirmarNuevoTipoUsuario').click(() => {
	let data = {
		nombre: $('#inputNombreTipoUsuario').val(),
		privilegios: asignadosPorCrear
	}
	if(!data.nombre) return alert('Debe ingresar el nombre');
	if(data.privilegios.length === 0) return alert('Debe asignar al menos un privilegio');
	setTimeout(() => {
		alert('Cambios guardados con éxito');
		$('.modal').modal('hide');
	}, 200);
});