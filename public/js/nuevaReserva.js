$(() => {
  $.datepicker.setDefaults($.datepicker.regional["es"]);

  $("#fechaInicio").datepicker({
    dateFormat: "dd/mm/yy",
  }).datepicker("setDate", 7);
  $("#fechaFin").datepicker({
    dateFormat: "dd/mm/yy"
  }).datepicker("setDate", "today");

  $('#btnConsultarDisponibilidad').click(() => {
    $().alert();
    $('.alert').show('show');
  });
});