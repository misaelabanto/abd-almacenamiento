$(() => {
  $.datepicker.setDefaults($.datepicker.regional["es"]);

  $("#fecha").datepicker({
    dateFormat: "dd/mm/yy",
  }).datepicker("setDate", 7);

  $('#btnSolicitarRetiro').click(() => {
    $('.modal').modal('show');
  });
  $('#confirmarSolicitudIngreso').click(() => {
    setTimeout(() => {
      alert('Solicitud enviada con éxito');
      $('.modal').modal('hide');
    }, 200);
  });
});