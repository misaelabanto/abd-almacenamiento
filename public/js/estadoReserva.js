let reserva = {
  'RE0012': {
    'Candelas' : [
      {
        unidad: 'Caja de 10 unidades',
        cantidad: 50
      },
      {
        unidad: 'Caja de 50 unidades',
        cantidad: 40
      },
      {
        unidad: 'Caja de 100 unidades',
        cantidad: 30
      },
      {
        unidad: 'Caja de 200 unidades',
        cantidad: 20
      },
    ],
    'Bengalas' : [
      {
        unidad: 'Caja de 10 unidades',
        cantidad: 100
      },
      {
        unidad: 'Caja de 50 unidades',
        cantidad: 80
      },
      {
        unidad: 'Caja de 100 unidades',
        cantidad: 12
      }
    ],
    'Cohetes' : [
      {
        unidad: 'Caja de 10 unidades',
        cantidad: 200
      },
      {
        unidad: 'Caja de 50 unidades',
        cantidad: 120
      },
      {
        unidad: 'Caja de 100 unidades',
        cantidad: 80
      }
    ],
    'Truenos' : [
      {
        unidad: 'Caja de 10 unidades',
        cantidad: 150
      },
      {
        unidad: 'Caja de 50 unidades',
        cantidad: 100
      }
    ],
  }
};

$('#bienAlmacenado').change(function() {
  let bienes = $(this).val();
  poblarTabla(bienes);
});

function poblarTabla(bienes) {
  let tableBienes = $('#tableBienes tbody').html('');
  bienes.forEach(bien => {
    reserva['RE0012'][bien].forEach(row => {
      tableBienes.append(`
      <tr>
        <td>${bien}</td>
        <td>${row.unidad}</td>
        <td>${row.cantidad}</td>
      </tr>
      `)
    });
  });
}

function imprimirAhora() {
  window.print();
}

function enviarAmiCorreo() {
  document.location.href = "mailto:josemisaelabanto@gmail.com";
}