let rutas = {
  'R1': [
    {
      codRuta: 'R1',
      codVector: 'V1',
      secuencia: 1
    }
  ],
  'R2':[
    {
      codRuta: 'R2',
      codVector: 'V2',
      secuencia: 1
    },
    {
      codRuta: 'R2',
      codVector: 'V3',
      secuencia: 2
    },
    {
      codRuta: 'R3',
      codVector: 'V4',
      secuencia: 3
    }
  ],
  'R3': [
    {
      codRuta: 'R3',
      codVector: 'V4',
      secuencia: 1
    },
    {
      codRuta: 'R3',
      codVector: 'V5',
      secuencia: 2
    },
    {
      codRuta: 'R3',
      codVector: 'V6',
      secuencia: 3
    }
  ]
};

function agregarNodo() {

}

function agregarVector() {

}

function agregarRuta() {

}

function guardarNodos() {

}

function guardarVectores() {

}

function guardarRutas() {

}

function verDetalleRuta(codRuta) {
  let relacionRutaVector = $('#relacionRutaVector tbody').html('');
  rutas[codRuta].forEach(r => {
    relacionRutaVector.append(`
    <tr>
      <td>${r.codRuta}</td>
      <td>${r.codVector}</td>
      <td>${r.secuencia}</td>
    </tr>
    `)
  });
}