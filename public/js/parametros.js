let cont = 5, contPeligrosidad = 4;
$('#btnAgregarTipoTemperatura').click(() => {
  agregarTipoTemperatura();
});

$('#btnAgregarTipoPeligrosidad').click(() => {
  agregarTipoPeligrosidad();
});

function addListeners() {
  $('input[type="checkbox"]').change(function() {
    enableButtonSave();
  });

  $('td[contenteditable=""]').keyup(function() {
    enableButtonSave();
  });
}

$(addListeners);

function enableButtonSave() {
  $('#btnGuardarTodo').prop('disabled', false);
}

function agregarTipoTemperatura() {
  let tableTiposTemperatura = $('#tableTiposTemperatura tbody');
  tableTiposTemperatura.append(`
    <tr id="ttemp-${cont}">
      <td contenteditable="">TE0${cont}</td>
      <td contenteditable=""></td>
      <td contenteditable="" class="text-right">0.00 °C</td>
      <td contenteditable="" class="text-right">0.00 °C</td>
      <td><div class="custom-control custom-checkbox mr-sm-2">
        <input class="custom-control-input" id="checkEstado${cont}" type="checkbox" checked="checked" />
        <label class="custom-control-label" for="checkEstado${cont}">Activo</label>
       </div></td>
       <td class="text-center"><button class="btn btn-danger" onclick="$('#ttemp-${cont++}').remove()">&times;</button></td>
    </tr>
  `);
  addListeners();
}

function agregarTipoPeligrosidad() {
  let tableTipoPeligrosidad = $('#tableTipoPeligrosidad tbody');
  tableTipoPeligrosidad.append(`
    <tr id="tpel-${contPeligrosidad}">
      <td>PEL0${contPeligrosidad}</td>
      <td>
        <select class="custom-select">
          <option value="">Mortal</option>
          <option value="">Muy peligroso</option>
          <option value="">Peligroso</option>
          <option value="">Poco peligroso</option>
          <option value="">Sin riesgo</option>
        </select>
      </td>
      <td>
        <select class="custom-select">
          <option value="">Muy inflamable</option>
          <option value="">Inflamable</option>
          <option value="">Poco inflamable</option>
          <option value="">Muy poco inflamable</option>
          <option value="">No se inflama</option>
        </select>
      </td>
      <td>
        <select class="custom-select">
          <option value="">Explosivo repentino</option>
          <option value="">Explosivo al chocar o calentar</option>
          <option value="">Inestable al cambio químico violento</option>
          <option value="">Inestable al calentar</option>
          <option value="">Estable</option>
        </select>
      </td>
      <td>
        <select class="custom-select">
          <option value="">Ninguno</option>
          <option value="">Oxidante</option>
          <option value="">Corrosivo</option>
          <option value="">Radioactivo</option>
          <option value="">No usar agua</option>
          <option value="">Riesgo biológico</option>
        </select>
      </td>
      <td>
        <div class="custom-control custom-checkbox mr-sm-2">
          <input class="custom-control-input" id="checkEstadoPeligrosidad${contPeligrosidad}" type="checkbox" checked="checked" />
          <label class="custom-control-label" for="checkEstadoPeligrosidad${contPeligrosidad++}">Activo</label>
        </div>
      </td>
      <td class="text-center"><button class="btn btn-danger" onclick="$('#tpel-${cont++}').remove()">&times;</button></td>
    </tr>
  `);
  addListeners();
}

$('#btnGuardarTodo').click(() => {
  $('td[contenteditable=""]').each((ind, elem) => {
    console.log(elem);
    if($(elem).text() === ''){
      alert('Alguno de sus campos es inválido. Vuelva a intentar');
      return false;
    }
  });
});