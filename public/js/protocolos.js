let cont = 0;
function agregarProtocolo() {
  let tableProtocolos = $('#tableProtocolos tbody');
  tableProtocolos.append(`
  <tr id="prot-${cont}">
    <td>PROT0${cont}</td>
    <td>
      <select class="custom-select ">
        <option>Ninguna</option>
        <option>Periódica</option>
        <option>Única</option>
        <option>Eventual</option>
      </select>
    </td>
    <td>
      <select class="custom-select ">
        <option>Sin revisión</option>
        <option>Estándar</option>
        <option>Minuciosa</option>
      </select>
    </td>
    <td>
      <select class="custom-select ">
        <option>Estándar</option>
      </select>
    </td>
    <td>
      <select class="custom-select ">
        <option>Paletización Convencional</option>
        <option>Paletización Compacta</option>
        <option>Push back con carros</option>
        <option>Push back con rodillos</option>
        <option>Pallet Shuttle</option>
        <option>Dinámica con rodillos</option>
        <option>Pallet Shuttle automático</option>
        <option>Dinámica con rodillos automát.</option>
      </select>
    </td>
    <td>
      <select class="custom-select ">
        <option>Ninguna</option>
        <option>Periódica</option>
        <option>Única</option>
        <option>Eventual</option>
      </select>
    </td>
    <td>
      <select class="custom-select ">
        <option>Ninguno</option>
        <option>Por unidad</option>
        <option>Por sub-grupo</option>
      </select>
    </td>
    <td>
      <select class="custom-select ">
        <option>Ninguno</option>
        <option>Agrupado de lotes</option>
        <option>Agrupado de unidades</option>
      </select>
    </td>
    <td>
      <select class="custom-select ">
        <option>Ninguno</option>
        <option>Estándar</option>
      </select>
    </td>
    <td>
      <button class="btn btn-danger" onclick="$('#prot-${cont++}').remove()">&times;</button>
    </td>
  </tr>
  `)
}

function guardarProtocolos() {
  let isSure = prompt('¿Está seguro de guardar los protocolos?');
  if(isSure) {
    setTimeout(() => {
      alert('Protocolos guardados con éxito');
    }, 200)
  }
}